import React from 'react';
import './App.css';

// 导入Provider组件 利用这个组件包裹我们的结构 从而能够达到统一维护store的效果

// Provider组件实现
// 1.导入Provider组件,在react-redux中进行导入
// 2.需要利用Provider组件 对整个结构进行包裹
// 3.给Provider组件设置 store属性 这个值就是我们同各国creacteStore构建出来的store 实例对象
import { Provider } from 'react-redux'

// 导入store
import store from './store'

// 导入组件
import ComA from './pages/ComA'
import ComB from './pages/ComB'

function App() {
  return (
    //  Provider包裹在根组件的最外层  接收store做为props 
    <Provider store={store}>  
      <div className="App">
        <ComA></ComA>
        <ComB></ComB>
      </div>
    </Provider>
  );
}

export default App;
