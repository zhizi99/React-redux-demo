import React, { Component } from 'react'
import { connect } from 'react-redux';

export class ComB extends Component {
    render() {
        return (
            <div>
               {this.props.count}
            </div>
        )
    }
}


// 接收两个参数

const mapStateToProps = state => {
    return state;
}
// ComB接收state
// 1.导入connect 方法
// 2.利用connect对组件进行加强
// 3.ComB属于是接收方，就需要实现connect的第一个参数
// 4.mapStateToProps  里面的第一个参数就是state
// 5.把这个state进行return 才能在组件内部获取到最新的数据
// 6.ComB是否可以拿到数据 关键点在于reducer
// 7.只有reducer里面返回来新的sstate的时候 我们才能获取到数据








// B属于接收方 需要实现connect 方法的第一个参数
export default connect(mapStateToProps)(ComB);