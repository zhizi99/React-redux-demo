import React, { Component } from 'react'

import {connect} from 'react-redux'

export  class ComA extends Component {
    handleClick=()=>{
        console.log(this.props)
        // 发送action
        this.props.sendAction()
    }
    render() {
        return (
            <div>
               <button onClick={this.handleClick}>+</button>
            </div>
        )
    }
}

// 这个函数要有一个返回值 返回值是一个对象
// @param {*} dispatch
const mapDispatchToProps =(dispatch)=>{
    return {
        sendAction:()=>{
        // 利用dispatch 发送一个action
        // 传递action对象 需要定义一个type属性
            dispatch({
               type:`add_action`
            })
        }
    }
}

console.log(mapDispatchToProps())
// ComA 发送action
// 1.导入connect
// 2.利用connect 对组件进行加强
//  connect(要接收数组的函数,要发送action的函数)(放入要加强的组件)
// 3.实现connect 第二个参数
// 4.构建一个函数 mapDispatchToProps(dispatch)
// dispatch:就是用来发送给action 的
// 5.在这个函数里面就可以返回一个对象
// key：是方法名
// value：调用dispatch去发送action
// 6.组件的内容 就可以通过this.props来拿到这个方法




// A是发送方 实现connect 第二个参数
export default connect(null,mapDispatchToProps)(ComA) ;
